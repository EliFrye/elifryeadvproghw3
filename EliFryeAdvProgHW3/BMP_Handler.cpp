#include "stdafx.h"
#include "BMP_Handler.h"
#include <fstream>
#include <istream>
#include <ostream>
#include <iostream>
const int NUM_COLORS = 3;
BMP_API unsigned char* BMP_Handler::loadBMP(const char* fileName, int& w, int& h)
{
	/*char signature[2];
	char fileSize[4];
	char throwAway[4];//To read in things that we are not going to use.
	char throwAway2[4];
	char headerSize[4];
	char width[4];
	char height[4];
	std::ifstream fin;
	fin.open(fileName);
	if(fin.fail())
		exit(1);
	fin.read(signature,2);
	fin.read(fileSize,4);
	fin.read(throwAway,4);
	fin.read(throwAway2, 4);
	fin.read(headerSize,4);
	fin.read(width,4);
	fin.read(height,4);
	w = *(reinterpret_cast<int*>  (width));
	h = *(reinterpret_cast<int*>  (height));
	char* ret = new char[NUM_COLORS*w*h];//The pixel array.
	int index=0;//To keep track of which position in ret to send char to next.
	for(int i=0;i<NUM_COLORS*8;i++)
		fin.read(throwAway,4);
	for(int i=0;i<w;i++)
	{
		for(int j=0;j<h;j++)
		{
			for(int k=0;k<NUM_COLORS+1;k++)
			{
				if(k!=NUM_COLORS)
				{
					fin.read(&ret[index],1);
					char c=ret[index];
					index++;
				}
				else
				{
					fin.read(throwAway,1);
				}
				
			}
		}
		for(int j=0;j<(4-((NUM_COLORS*w)%4))%4;j++)
		{
			index++;
		}
	}
	fin.close();
	return reinterpret_cast<unsigned char*> (ret);*/
	int sp=0;
	std::ifstream fin;
	fin.open(fileName);
	char throwAway[4];
	char width[4];
	char height[4];
	fin.seekg(18);
	fin.read(width,4);
	fin.read(height,4);
	w = *(reinterpret_cast<int*>  (width));
	h = *(reinterpret_cast<int*>  (height));
	char* ret = new char[(NUM_COLORS+1)*w*h+122+122];//The pixel array.
	fin.seekg(0);
	int index=0;
	for(int i=0;i<w*h*(NUM_COLORS+1);i++)
	{
		fin.read(&ret[index],1);
		char c=ret[index];
		index++;
	}
	for(int i=0;i<122;i++)
	{
		ret[index]=ret[i];
		char it = ret[index];
		index++;
	}
	/*unsigned char* finalRet = */return reinterpret_cast<unsigned char*> (ret);
	//return finalRet;
	fin.seekg(0);
}

BMP_API void BMP_Handler::saveBMP(const char*fileName, const unsigned char* pixArr, int w, int h)
{
	
	std::ofstream fout;
	
	char next = '\0';
	fout.open(fileName);
	int index=(h*w*(NUM_COLORS+1));
	for(int i=index;i<index+122;i++)
	{
		next=pixArr[i];
		fout.write(&next,1);
	}
	index=122;
	for(int i=index;i<h*w*(NUM_COLORS+1);i++)
	{
		next=pixArr[i];
		fout.write(&next,1);
	}
	/*
	char signature[2]={'B','M'};
	char* fileSize ;
	int numericalFileSize = 122 + (h*(NUM_COLORS+1)*(w+((4-((w*NUM_COLORS)%4)))%4)); //The size of the file as an integer.
	fileSize = reinterpret_cast<char*> (&numericalFileSize);
	char throwAway[4]={'\0','\0','\0','\0'};//To write parts we are ignoring.
	char* fileOffset;
	int fileOffsetNumeric = 108;
	fileOffset = reinterpret_cast<char*> (&fileOffsetNumeric);
	char* headerSize;
	int headerSizeNumeric = 108;
	headerSize = reinterpret_cast<char*> (&headerSizeNumeric);
	char* width;
	int widthNumeric = w;
	width = reinterpret_cast<char*> (&widthNumeric);
	char* height;
	int heightNumeric = h;
	height = reinterpret_cast<char*> (&heightNumeric);
	std::ofstream fout;
	fout.open(fileName);
	if(fout.fail())
		exit(1);
	fout.write(signature,2);
	fout.write(fileSize,4);
	fout.write(throwAway,4);
	fout.write(fileOffset,4);
	fout.write(headerSize,4);
	fout.write(width,4);
	fout.write(height,4);
	for(int i=0;i<NUM_COLORS*8;i++)
		fout.write(throwAway,4);
	int index=0;
	for(int i=0;i<w;i++)
	{
		for(int j=0;j<h;j++)
		{
			for(int k=0;k<NUM_COLORS+1;k++)
			{
				if(k!=NUM_COLORS)
				{
				fout.write(reinterpret_cast<const char*>(&pixArr[index]),1);
				index++;
				}
				else
					fout.write(throwAway,1);
			}
		}
		for(int j=0;j<(4-((NUM_COLORS*w)%4))%4;j++)
		{
			fout.write(throwAway,4);
			index++;
		}
	}
	fout.close();
	*/
	/*std::ofstream fout;
	fout.open(fileName);
	if(fout.fail())
		exit(1);
	//char output[] = {0x42, 0x4d, 0x7b, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,0x00, 0x86, 0x00,0x00,0x00,0x6c,0x00,0x00,0x00,0x80, 0x00,0x00,0x00,0x01,0x00,0x00,0x00,0x01,0x00,0x00,0x00,0x03,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xff,0x00,0x00,0x00,0xff,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xff,0x00,0x00,0x00,0x00,0xff,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xff,0x42,0x47,0x52,0x73,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x00,0x00,0x00,0x01,0x00,0x00,0x00,0x01,0x00,0x00,0x00,0xff,0x00,0x00,0x00,0x00};
	unsigned char output[] = {0x00,0x42,0x00,0xBD,0xF8};
	//char c[] = {reinterpret_cast<char>(0xFF)};
	unsigned char d[] = {0xFF};
	//unsigned char* e = reinterpret_cast<unsigned char*> (c);
	//if(d[1]==e[1])
	//	std::cout << "it works.";
	char* finalOutput = reinterpret_cast<char*> (output);
	fout.write(finalOutput,5);*/
}